package com.tugas.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name= "product_table")
public class Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	@Column()
	private long id;
	
	@Column(length = 10)
	private String code;
	
	@Column(length = 50)
	private String name;
	
	@Column(length = 20)
	private String type;
	
	@Column()
	private double price;
	
	@Column()
	private int stock;
	
	public Product() {
		
	}

	public Product(String code, String name, String type, double price, int stock) {
		this.code = code;
		this.name = name;
		this.type = type;
		this.price = price;
		this.stock = stock;
	}
	
	public String getCode() {
		return code;
	}
	
	public String getName() {
		return name;
	}
	
	public String getType() {
		return type;
	}
	
	public double getPrice() {
		return price;
	}
	
	public int getStock() {
		return stock;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	public String toString() {
		return "Product data of #" + code + " " + name + " - type: " + type + " - price: " + price + " - current stock: " + stock;
	}

}