<%@ page import="com.tugas.domain.Product"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form action="product.php?action=update" method="post">
		<h5>Product Name:</h5>
		<br>
		<input name="product_name" type="text" value="${product_detail.name}" />
		<br>
		<h5>Product Code:</h5>
		<br>
		<input name="product_code" type="text" value="${product_detail.code}" />
		<br>
		<h5>Product Type:</h5>
		<br>
		<input name="product_type" type="text" value="${product_detail.type}" />
		<br>
		<h5>Price:</h5>
		<br>
		<input name="product_price" type="number" value="${product_detail.price}" />
		<br>
		<h5>Stock:</h5>
		<br>
		<input name="product_stock" type="number" value="${product_detail.stock}" />
		<br>
		<input name="product_id"" type="hidden" value="${product_detail.id}" />
		<input type="submit" value="Save" />
	</form>
</body>
</html>