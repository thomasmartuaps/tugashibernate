package com.tugas.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tugas.domain.Product;
import com.tugas.services.ProductService;

/**
 * Servlet implementation class ProductServlet
 */
@WebServlet("/product.php")
public class ProductServlet extends HttpServlet {
	private ProductService service = new ProductService();
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String action = request.getParameter("action");
		PrintWriter out = response.getWriter();
		try {
			if ("list".equals(action)) {
				List<Product> products = service.findProducts(0);
				request.setAttribute("product_list", products);
				request.getRequestDispatcher("/ProductList.jsp").forward(request, response);
			} else if ("view".equals(action)) {
				long id = Long.parseLong(request.getParameter("product_id"));
				List<Product> products = service.findProducts(id);

				request.setAttribute("product_detail", products.get(0));
				request.getRequestDispatcher("/ProductDetails.jsp").forward(request, response);
			} else if ("form".equals(action)) {
				out.println("redirect to create product page.");
			}
			else {
				;
				out.println("not found.");
			}
			
		} catch(ClassNotFoundException e) {
			out.println("<h1>Class Not found.</h1>");
			e.printStackTrace();
		} catch(SQLException e) {
			out.println("<h1>DB Error.</h1>");
			e.printStackTrace();
		}
//		PrintWriter out = response.getWriter();
//		System.out.println(products);
//		out.println("<table width=90% border=2>");
//		out.println("<tr>");
//		out.println("<td><bold>Name</bold></td>");
//		out.println("<td><bold>Code</bold></td>");
//		out.println("<td><bold>Type</bold></td>");
//		out.println("<td><bold>Price</bold></td>");
//		out.println("<td><bold>Stock</bold></td>");
//		out.println("</tr>");
//		for (Product product : products) {
//			out.println("<tr>");
//			out.println("<td>" + product.getName() + "</td>");
//			out.println("<td>" + product.getCode() + "</td>");
//			out.println("<td>" + product.getType() + "</td>");
//			out.println("<td>" + product.getPrice() + "</td>");
//			out.println("<td>" + product.getStock() + "</td>");
//			out.println("</tr>");
//		}
//		out.println("</table>");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String action = request.getParameter("action");
		
		String name = request.getParameter("product_name");
		String code = request.getParameter("product_code");
		String type = request.getParameter("product_type");
		double price = Double.parseDouble(request.getParameter("product_price"));
		int stock = Integer.parseInt(request.getParameter("product_stock"));
		String productId = request.getParameter("product_id");
		long id = productId != null ? Long.parseLong(productId) : 0;
		System.out.println(name);
		System.out.println(code);
		System.out.println(type);
		System.out.println(price);
		System.out.println(stock);
		System.out.println(id);
		
		
		PrintWriter out = response.getWriter();

		try {
			if ("update".equals(action)) {
				Product updateProduct = new Product(name, code, type, price, stock);
				int result = service.update(updateProduct, id);
				if (result > 0) {				
					out.println("<h1>Product modified!</h1>");
					out.println("<div>#" + code + " " + name + " " + "product type: " + type + "</div>");
					out.println("<br>");
					out.println("<a href='product.php?action=list'>Back to table</a>");
				} else {
					out.println("<h1>Failed to update  product!</h1>");
					out.println("<div>SQL Exception</div>");
					out.println("<br>");
					out.println("<a href='product.php?action=view&product_id=" + id + " >Back to form</a>");
					
				}
				
			} else if ("create".equals(action)) {
				Product newProduct = new Product(name, code, type, price, stock);
				int result = service.save(newProduct);
				if (result > 0) {				
					out.println("<h1>Product added!</h1>");
					out.println("<div>#" + code + " " + name + " " + "product type: " + type + "</div>");
					out.println("<br>");
					out.println("<a href='product.php?action=list'>Back to table</a>");
				} else {
					out.println("<h1>Failed to create product!</h1>");
					out.println("<div>SQL Exception</div>");
					out.println("<br>");
					out.println("<a href='createproduct.html'>Back to form</a>");
					
				}
			}
			
		} catch(ClassNotFoundException e) {
			out.println("<h1>Cannot connect to db</h1>");
			e.printStackTrace();
		} catch(SQLException e) {
			out.println("<h1>SQL Error.</h1>");
			e.printStackTrace();
		}
		

	}

}
