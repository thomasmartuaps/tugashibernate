<%@ page import="java.util.List"%>
<%@ page import="com.tugas.domain.Product"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="WEB-INF/myTag.tld" prefix="p" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1>Product List</h1>
<table width="80%" border=2>
	<tr>
		<th>Code</th>
		<th>Name</th>
		<th>Type</th>
		<th>Action</th>
	</tr>
	<c:forEach items="${product_list}" var="product">
	<tr>
	<td><c:out value="${product.code}"></c:out></td>
	<td><c:out value="${product.name}"></c:out></td>
	<td><c:out value="${product.type}"></c:out></td>
	<td><a href="product.php?action=view&product_id=${product.id}">View</a></td>
	</tr></c:forEach>
</table>
<script>
</script>
</body>
</html>