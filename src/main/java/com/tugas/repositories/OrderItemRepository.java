package com.tugas.repositories;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.tugas.domain.OrderItem;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class OrderItemRepository {
	private Connection conn;
	public OrderItemRepository(Connection conn) {
		// TODO Auto-generated method stub
		this.conn = conn;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public void print(OrderItem orderItem) {
		System.out.println(orderItem.getCode());
		System.out.println(orderItem.getName());
	}
	public void save(OrderItem orderItem) throws SQLException {
		String sqlOrderItem = "insert into order_item_table(code, name, type, price, qty, order_id_fk) values(?,?,?,?,?,?)";
		
		PreparedStatement stm = conn.prepareStatement(sqlOrderItem);
		int Index = 0;
		stm.setString(++Index, orderItem.getCode());
		stm.setString(++Index, (orderItem.getName()));
		stm.setString(++Index, (orderItem.getType()));
		stm.setDouble(++Index, (orderItem.getPrice()));
		stm.setInt(++Index, (orderItem.getQty()));
		stm.setInt(++Index, (orderItem.getOrderId()));
		
		int result = stm.executeUpdate();

		return;
	}
	public List<OrderItem> findAll() throws SQLException {
		String sql = "select * from order_item_table";
		
		PreparedStatement stm = conn.prepareStatement(sql);
		
		ResultSet res = stm.executeQuery();
		List<OrderItem> list = new ArrayList<>();
		while (res.next()) {
			list.add(new OrderItem(res.getString("code"), res.getString("name"), res.getString("type"), res.getDouble("price"), res.getInt("qty"), res.getInt("order_id_fk")));
		}
		return list;
	}
	public List<OrderItem> findByOrderId(int orderId) throws SQLException {
		String sql = "select * from order_item_table WHERE order_id_fk = ?";
				
		PreparedStatement stm = conn.prepareStatement(sql);
		stm.setInt(1, orderId);
		
		ResultSet res = stm.executeQuery();
		List<OrderItem> list = new ArrayList<>();
		while (res.next()) {
			list.add(new OrderItem(res.getString("code"), res.getString("name"), res.getString("type"), res.getDouble("price"), res.getInt("qty"), res.getInt("order_id_fk")));
		}
		return list;
	}
}
