package com.tugas.domain;

import java.util.Date;

public class Order {
	int id;
	long createdAt = new Date().getTime();
	
	public Order(int id, long createdAt) {
		this.id = id;
		if (createdAt > 0) {
			this.createdAt = createdAt;
		}
	}
	
	public int getId() {
		return id;
	}
	
	public long getCreatedAt() {
		return createdAt;
	}
	
	public String toString() {
		return id + " - " + new Date(createdAt).toString();
	}
}
