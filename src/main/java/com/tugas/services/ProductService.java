package com.tugas.services;

import java.sql.SQLException;
import java.util.List;

import com.tugas.domain.Product;
import com.tugas.repositories.ProductRepository;

public class ProductService {
	public ProductService() {
		
	}
	public int save(Product product) throws SQLException, ClassNotFoundException {
		ProductRepository productRepo = new ProductRepository();
		int result = 0;
		try {
			productRepo.save(product);
			result = 1;
		} catch (SQLException e) {
			System.out.println("SQL error, rolling back...");
			e.printStackTrace();
			result = 0;
		}
		System.out.println("Selesai create product");
		return result;
	}
	public int update(Product product, long id) throws SQLException, ClassNotFoundException {
		ProductRepository productRepo = new ProductRepository();
		int result = 0;
		try {
			productRepo.update(product, id);
			result = 1;
		} catch (SQLException e) {
			System.out.println("SQL error, rolling back...");
			e.printStackTrace();
			
		}
		System.out.println("Selesai update product");
		return result;
	}
	public List<Product> findProducts(long id) throws SQLException, ClassNotFoundException {

		ProductRepository productRepo = new ProductRepository();
		List<Product> products = null;
		try {
			if (id > 0) {
				products = productRepo.findById(id);
			} else {
				products = productRepo.findAll();	
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return products;
	}
}
