package com.tugas.repositories;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import com.tugas.domain.Product;
import com.tugas.util.HibernateUtil;

public class ProductRepository {
	public ProductRepository() {
		// TODO Auto-generated method stub
	}
	
	public void print(Product product) {
		System.out.println(product.getCode());
		System.out.println(product.getName());
	}
	public void save(Product product) throws SQLException {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		session.beginTransaction();
		
		session.persist(product);
		
		session.getTransaction().commit();
	}
	public void update(Product product, long id) throws SQLException {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		session.beginTransaction();
		product.setId(id);
		
		session.update(product);
		
		session.getTransaction().commit();

	}
	public List<Product> findAll() throws SQLException {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		session.beginTransaction();

		List<Product> products = session.createQuery("from Product").list();
		return products;
	}
	public List<Product> findById(long id) throws SQLException {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		session.beginTransaction();

		List<Product> products = session.createQuery("from Product p where p.id = :id ")
				.setParameter("id", id)
				.list();
		return products;
	}
}
